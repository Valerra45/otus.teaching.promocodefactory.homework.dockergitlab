﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        private readonly PromoCodeContext _context;

        public EfRepository(PromoCodeContext context)
        {
            _context = context;
        }

        public async Task AddAsync(T obj)
        {
            await _context.Set<T>().AddAsync(obj);
        }

        public async Task AddRangeAsync(IEnumerable<T> obj)
        {
            await _context.Set<T>().AddRangeAsync(obj);
        }

        public void Delete(T obj)
        {
            _context.Set<T>().Remove(obj);
        }

        public void DeleteRange(IEnumerable<T> obj)
        {
            _context.Set<T>().RemoveRange(obj);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _context.Set<T>()
                .ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _context.Set<T>()
                     .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            return await _context.Set<T>()
                .FirstOrDefaultAsync(predicate);
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            return await _context.Set<T>()
                .Where(x => ids.Contains(x.Id))
                .ToListAsync();
        }

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            return await _context.Set<T>()
                .Where(predicate)
                .ToListAsync();
        }

        public void Update(T obj)
        {
            _context.Set<T>().Update(obj);
        }

        public void UpdateRange(IEnumerable<T> obj)
        {
            _context.Set<T>().UpdateRange(obj);
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}

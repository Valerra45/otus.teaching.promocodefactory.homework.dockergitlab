using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.IUnitOfWorks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public CustomersController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Получить данные всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _unitOfWork.GetRepository<Customer>()
                .GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            return Ok(response);
        }

        /// <summary>
        /// Получить данные клиента по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _unitOfWork.GetRepository<Customer>()
                .GetByIdAsync(id);

            var response = new CustomerResponse()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                PrefernceResponses = customer.Preferences.Select(x => new PrefernceResponse()
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList()
            };

            return Ok(response);
        }

        /// <summary>
        /// Создание клиента с его предпочтениями
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var preferences = await _unitOfWork.GetRepository<Preference>()
                .GetRangeByIdsAsync(request.PreferenceIds);

            var customer = new Customer()
            {
                Id = Guid.NewGuid(),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = (List<Preference>)preferences
            };

            await _unitOfWork.GetRepository<Customer>().AddAsync(customer);

            await _unitOfWork.SaveChangesAsync();

            return CreatedAtAction(nameof(GetCustomerAsync), new { id = customer.Id }, customer.Id);
        }

        /// <summary>
        /// Обновить клиента с его предпочтениями
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _unitOfWork.GetRepository<Customer>()
                .GetByIdAsync(id);

            if (customer == null)
            {
                return NotFound();
            }

            var preferences = await _unitOfWork.GetRepository<Preference>()
                .GetRangeByIdsAsync(request.PreferenceIds);

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
            customer.Preferences.Clear();
            customer.Preferences.AddRange(preferences);

            _unitOfWork.GetRepository<Customer>().Update(customer);

            await _unitOfWork.SaveChangesAsync();

            return CreatedAtAction(nameof(GetCustomerAsync), new { id = customer.Id }, customer.Id);
        }

        /// <summary>
        /// Удалить клиента клиента с выданными ему промокодами
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _unitOfWork.GetRepository<Customer>()
                .GetByIdAsync(id);

            if (customer == null)
            {
                return NotFound();
            }

            _unitOfWork.GetRepository<Preference>()
                .DeleteRange(customer.Preferences);

            _unitOfWork.GetRepository<Customer>()
                .Delete(customer);

            await _unitOfWork.SaveChangesAsync();

            return NoContent();
        }
    }
}

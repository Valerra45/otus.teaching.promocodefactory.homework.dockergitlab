﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.IUnitOfWorks;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferenceController
        : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public PreferenceController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Получить все предпочтения
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<PrefernceResponse>> GetPreferencesAsync()
        {
            var preferences = await _unitOfWork.GetRepository<Preference>()
                .GetAllAsync();

            var responce = preferences.Select(x => new PrefernceResponse()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            return Ok(responce);
        }
    }
}
